<?php

    $structure = [
        'config/database.php' => 'file',
        'logs' => 'dir'
    ];


    // Loop Structure
    foreach( $structure as $d => $p ) {

        // Create Directories
        if( $p == 'dir' ) {
            if( !is_dir($d) ) { mkdir($d); }
            if( !is_dir($d) ) { die($d); }
        }

        // Create Files
        if( $p == 'file' ) {
            if( !file_exists($d) ) { touch($d); }
            if( !file_exists($d) ) { die($d); }
        }

    }


    // Composer Install Packages
    shell_exec('composer install');


    // Done
    touch('.init-done');
