<?php

    // Error Reporting
    include 'config/error_handle.php';

    // Load Database Settings
    include 'config/database.php';

    // Load Paths
    include 'config/paths.php';

    // Load Composer Modules
    include 'vendor/autoload.php';

    // Set timezone
    date_default_timezone_set('Asia/Karachi');

    // Initialize Database Connection
    $mysqli = new mysqli (T_DBHOST, T_DBUSER, T_DBPASS, T_DBNAME);
    $dbConn = new MysqliDb ($mysqli);

    // Load Models
    foreach( scandir('classes') as $class ) {
        $paths_ext = pathinfo($class);
        if( $paths_ext['extension'] == 'php' ) {
            include REL_PATH . 'classes/'.$class;
        }
    }