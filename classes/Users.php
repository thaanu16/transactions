<?php

/**
 * Get user by username
 * @param string $username
 * @return array
 */
function getUser( $username )
{
    global $dbConn;
    $dbConn->where('username', $username);
    $r = $dbConn->get('users')[0];
    if( $dbConn->count > 0 ) {
        return $r;
    }
    return array();
}