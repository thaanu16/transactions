<?php

    $data = [
        'status'    => false,
        'message'   => 'Statement not recieved'
    ];

    // Check if statement was sent
    if( isset($_POST['statement']) && $_POST['statement'] != '' ) :

        $str = $_POST['statement'];
        
        // Card number
        preg_match('/Transaction from ([0-9]*)/', $str, $m);
        $cardID = $m[1];

        // Date Time - Date Potion
        preg_match('/on ([0-9]*\/[0-9]*)/', $str, $m);
        $d = $m[1];
        $d = str_replace('/', '-', $d) . '-' . date('Y');
        $d = date('Y-m-d', strtotime($d));

        // Date Time - Time Potion
        preg_match('/at ([0-9]*:[0-9]*:[0-9]*)/', $str, $m);
        $dateTime = $d . ' ' . $m[1];

        // Vendor
        preg_match('/at ([A-Z ]+)/', $str, $m);
        $vendor = $m[1];

        // Reference Number
        preg_match('/Reference No:([0-9]*)/', $str, $m);
        $refNo = $m[1];

        // Approval Code
        preg_match('/Approval Code:([0-9]*)/', $str, $m);
        $appNo = $m[1];

        // Currency and Amount
        preg_match('/for ([A-Za-z]*)([0-9\.]*)/', $str, $m);
        $currency   = $m[1];
        $amount     = $m[2];

        $dbInput = [
            'cardID'            => $cardID,
            'trans_datetime'    => $dateTime,
            'vendor'            => $vendor,
            'currency'          => $currency,
            'amount'            => $amount,
            'ref_number'        => $refNo,
            'approval_code'     => $appNo,
            'description'       => ($_POST['description'] == "" ? '' : $_POST['description']),
            'bml_statement'     => $str,
            'user_id'           => getUser($_POST['username'])['ID']
        ];

        $id = $dbConn->insert('transactions', $dbInput);

        if( $dbConn->count > 0 ) {
            $data = [
                'status' => true,
                'message' => 'Transaction completed'
            ];
        } else {
            $data['message'] = 'Incomplete transaction';
        }

    endif;

    // Return results
    echo json_encode($data);

