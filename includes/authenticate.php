<?php

    $response = [
        'status' => false
    ];

    if( !isset($_POST['username']) || !isset($_POST['password']) || $_POST['username'] == "" || $_POST['password'] == "" ) {
        $response = [
            'message' => 'You are not authorized'
        ];

        echo json_encode($response);
        exit;
        
    }

    $response ['message'] = 'User not found';

    // Check if user exists
    if( getUser($_POST['username'])['username'] != $_POST['username'] ) {
        echo json_encode($response); exit;
    }

    if( getUser($_POST['username'])['password'] != $_POST['password'] ) {
        echo json_encode($response); exit;
    }

    if( getUser($_POST['username'])['status'] != 'active' ) {
        $response['message'] = 'Inactive user';
        echo json_encode($response); exit;
    }


