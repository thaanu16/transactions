<?php

    // Stop app is init has not run
    if( !file_exists('.init-done') ) { die('First run INIT'); }

    // Configuration
    include 'config.php';

    // Load Composer Modules
    include 'vendor/autoload.php';

    // Authenticate
    include 'includes/authenticate.php';

    // Run the Application
    include 'App.php';