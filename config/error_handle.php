<?php

    if( !is_dir( dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "logs/") ) { mkdir(ROOT . DS . "logs/"); }
    if( !file_exists(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "logs/run-time-errors.log") ) { touch( dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "logs/run-time-errors.log" ); }
    if( !file_exists(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "logs/run-time-errors.log") ) { die('Unable to load logs'); }

    ini_set("log_errors", 1);
    ini_set("error_log", dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "logs/run-time-errors.log");